FROM php:5.6-apache

ENV DB_DRIVER   mysqli
ENV DB_HOSTNAME localhost
ENV DB_USERNAME root
ENV DB_PASSWORD password
ENV DB_DATABASE database
ENV DB_PREFIX   oc_
ENV HTTP_SERVER http://localhost/
ENV ADMIN_USERNAME  admin
ENV ADMIN_PASSWORD  admin
ENV ADMIN_EMAIL  youremail@example.com

RUN a2enmod rewrite
RUN apt-get update && apt-get install -y libpng12-dev libjpeg-dev libmcrypt-dev \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
    && docker-php-ext-install gd mcrypt mbstring mysqli zip

WORKDIR /var/www/html