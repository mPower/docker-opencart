#!/bin/bash

usermod -u 1000 www-data \
    && touch config.php \
    && touch admin/config.php \
    && php install/cli_install.php install \
        --db_hostname ${DB_HOSTNAME} \
        --db_username ${DB_USERNAME} \
        --db_password ${DB_PASSWORD} \
        --db_database ${DB_DATABASE} \
        --db_driver ${DB_DRIVER} \
        --db_prefix ${DB_PREFIX} \
        --username ${ADMIN_USERNAME} \
        --password ${ADMIN_PASSWORD} \
        --email ${ADMIN_EMAIL} \
        --http_server ${HTTP_SERVER} \
    && apache2-foreground